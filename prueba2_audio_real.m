%% Real-Time Audio Stream Processing
%
% The Audio System Toolbox provides real-time, low-latency processing of
% audio signals using the System objects audioDeviceReader and
% audioDeviceWriter.
%
% This example shows how to acquire an audio signal using your microphone,
% perform basic signal processing, and play back your processed
% signal.
%

%% Create input and output objects
deviceReader = audioDeviceReader;
deviceWriter = audioDeviceWriter('SampleRate',deviceReader.SampleRate);

%% Specify an audio processing algorithm
% For simplicity, only add gain.
process = @(x) x.*5;

%% Code for stream processing
% Place the following steps in a while loop for continuous stream
% processing:
%   1. Call your audio device reader like a function with no arguments to
%   acquire one input frame. 
%   2. Perform your signal processing operation on the input frame.
%   3. Call your audio device writer like a function with the processed
%   frame as an argument.

disp('Begin Signal Input...')
tic
n=20000;
scope = dsp.TimeScope(2,...
    'SampleRate',fileReader.SampleRate,...
    'TimeSpan',16,...
    'BufferLength',1.5e6,...
    'YLimits',[-1,1]);

B = [0.0675	0 -0.1349 0	0.0675]
A = [1	-1.9425	2.1192	-1.2167	0.4128]
while toc<20
    t = toc;
    mySignal = deviceReader();
    [myProcessedSignal,A,B] = IR(mySignal,A,B);
    
    deviceWriter(myProcessedSignal'*10);
    scope(mySignal,myProcessedSignal');
    %plot(myProcessedSignal,'.')
    
end
disp('End Signal Input')

release(deviceReader)
release(deviceWriter)   