function hw=hwfunction(x,a,b,fs)
n=length(x);
hw=ones(1,n);
for i=n
    
   A=polyval(a,exp(1i*2*pi*fs*(i-1)/n));
   B=polyval(b,exp(1i*2*pi*fs*(i-1)/n));
   hw(i)=B/A*x(i);
end
