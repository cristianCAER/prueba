function [f0,amp]=getfrecuencia(y,fs)
s=fft(y);
amp=max(abs(y));
A=abs(s);
na=length(A);
A=A(1:na/2)/na;
A(2:end)=2*A(2:end);
ejew=fs*(0:1:na/2-1)/na;
fase=angle(s);
fase=(fase(1:na/2)/na)*180/pi;
[b,i]=max(A);%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f0=ejew(i);%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,1,1)
semilogx(ejew,A)
xlabel('frecuencia en Hz')
ylabel('magnitud normalizada')
grid on
subplot(2,1,2)
semilogx(ejew,fase)
xlabel('frecuencia en Hz')
ylabel('fase en grados')
grid on
