clear all
clc
f=300;
fs=100000;
t=0:1/fs:4/f;
y1=sin(2*pi*f*t);
f2=10;
fs2=100000;
t2=0:1/fs2:1/f2;
y2=sin(2*pi*f2*t2); 
Y=getsuma(y1,y2);
plot(Y)