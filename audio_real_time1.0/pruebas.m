file = uigetfile;
fileReader = dsp.AudioFileReader(file);
file1 = uigetfile;
fileReader1 = dsp.AudioFileReader(file1);
deviceWriter = audioDeviceWriter('SampleRate',fileReader1.SampleRate);

tic;
while toc < 10
    mySignal = fileReader();
    mySignal1 = fileReader1();
    sum = getsuma(mySignal,mySignal1);
    deviceWriter(sum);
end

release(fileReader1);
release(fileReader);
release(deviceWriter);