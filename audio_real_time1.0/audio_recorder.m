deviceReader = audioDeviceReader
AR = dsp.AudioRecorder('OutputNumOverrunSamples',true);
in=inputdlg('Name recording');
ext = '.wav'
name = char(in)
file_name = strcat(name,ext)
AFW = dsp.AudioFileWriter(file_name,'FileFormat', 'wav');
disp('Speak into microphone now');
stop = 0;
scope1 = dsp.TimeScope(1,...
    'SampleRate',deviceReader.SampleRate, ...
    'LayoutDimensions',[1,1],...
    'NumInputPorts',1, ...
    'TimeSpan',0.1, ...
    'BufferLength',1.9e6,...
    'YLimits',[-0.3,0.3]);
app.UIAxes.XLim = [0 deviceReader.SampleRate*0.1];
app.UIAxes.YLim = [0 0.05];
app.UIAxes.BackgroundColor = [0.15 0.15 0.15];
app.UIAxes.XColor = [1 1 1];   
while stop ~= 1  
  stop = app.s
  [audioIn,nOverrun] = step(AR);
  step(AFW,audioIn);
  if nOverrun > 0
    fprintf('Audio recorder queue was overrun by %d samples\n'...
        ,nOverrun);
  end
  
  mySignal = audioIn(:,1);
        [f0,Am,f,m]=getFrec(mySignal,deviceReader.SampleRate);
        if f0 ~= 0 
            %app.Gauge.Value = app.v*Am;
            Amplitud = num2str(Am);
            app.Label_2.Text = Amplitud;
            Frecuencia = num2str(f0);
            app.Label.Text = Frecuencia;
            stem(app.UIAxes,f,m,'.','Color',[0.9 0.132 0.356]);     
        end  
  scope1(mySignal);
end

release(AR);
release(AFW);
fileReader = dsp.AudioFileReader(file_name);
deviceWriter = audioDeviceWriter('SampleRate',fileReader.SampleRate);

disp('Recording complete');   
scope = dsp.TimeScope(2,...
    'SampleRate',fileReader.SampleRate/8, ...
    'LayoutDimensions',[1,1],...
    'NumInputPorts',2, ...
    'TimeSpan',0.1, ...
    'BufferLength',1.9e6,...
    'YLimits',[-0.5,0.5]);

    %B = [0.2569	-0.7707	0.7707	-0.2569];
    %A = [1 -0.5772 0.4218 -0.0563];
play = 0
while play ~=1 
    play = app.p
    pause(0.1);
end 

if play == 1
    st = 1;
    while st ~= 0% ~isDone(fileReader)
        A = [str2num(app.a)];
        B = [str2num(app.b)];         
        mySignal = fileReader();
        st = mySignal(end);
        [f0,Am,f,m]=getFrec(mySignal,fileReader.SampleRate);
        if f0 ~= 0 
            %app.Gauge.Value = app.v*Am;
            Amplitud = num2str(Am);
            app.Label_2.Text = Amplitud;
            Frecuencia = num2str(f0);
            app.Label.Text = Frecuencia;
            stem(app.UIAxes,f,m,'.','Color',[0.9 0.132 0.356]);     
        end

        switch app.f
            case 'Eq. Dif'
                [myProcessedSignal1,A,B] = IR(mySignal(:,1),A,B);
                deviceWriter(app.v*myProcessedSignal1');
                scope(app.v*mySignal(:,1),app.v*myProcessedSignal1');
            case 'Res. Frec'
                s=fft(mySignal(:,1));
                size(s)
                hw=pasaalto(length(s),10000,fileReader.SampleRate);
                w=s.*hw';
                y2= real(ifft(w))-imag(ifft(w));
                deviceWriter(app.v*y2);
                scope(app.v*mySignal(:,1),app.v*y2);
            case 'No filter'
                switch  app.m
                    case 'Estereo'
                        deviceWriter(app.v*mySignal);
                    case 'Mono'
                        deviceWriter(app.v*mySignal(:,1));
                end                
                scope(app.v*mySignal(:,1),app.v*mySignal(:,1));
        end
    end    

end
    


release(scope);
release(fileReader);
release(deviceWriter);

