file = uigetfile;
fileReader = dsp.AudioFileReader(file);
deviceWriter = audioDeviceWriter('SampleRate',fileReader.SampleRate);
scope = dsp.TimeScope(2,...
    'SampleRate',fileReader.SampleRate, ...
    'NumInputPorts',2, ...
    'TimeSpan',0.1, ...
    'BufferLength',1.9e6,...
    'LayoutDimensions',[1,1],...
    'YLimits',[-0.3,0.3]);
app.UIAxes.XLim = [0 fileReader.SampleRate*0.1];
app.UIAxes.YLim = [0 0.2];
app.UIAxes.BackgroundColor = [0.15 0.15 0.15];
app.UIAxes.XColor = [1 1 1];  
play = 0;
while play ~=1 
    play = app.p
    pause(0.1);
end     

    %B = [0.2569	-0.7707	0.7707	-0.2569]
    %A = [1 -0.5772 0.4218 -0.0563]

 
if play == 1
    st = 1;
    stop = 0;
    while stop ~= 1 % ~isDone(fileReader)
    A = [str2num(app.a)];
    B = [str2num(app.b)];         
        %modo = app.m
        stop = app.s
        mySignal = fileReader();
        st = mySignal(end);
        switch app.g
            case 'No Graphic'
            
            case 'Magnitude'
                [f0,Am,f,m,fase]=getFrec(mySignal,fileReader.SampleRate);
                %[f0,Am]=getfrecuencia(mySignal,fileReader.SampleRate)
                %app.Gauge.Value = app.v*Am;
                if f0 ~= 0              
                    Amplitud = num2str(Am);
                    app.Label_2.Text = Amplitud;
                    Frecuencia = num2str(f0);
                    app.Label.Text = Frecuencia;
                    stem(app.UIAxes,f,m,'.','Color',[0.9 0.132 0.356]);     
                end
            case 'Phase'
        
        end
        switch app.f
            case 'Eq. Dif'
                [myProcessedSignal1,A,B] = IR(mySignal(:,1),A,B);
                deviceWriter(app.v*myProcessedSignal1');
                scope(app.v*mySignal(:,1),app.v*myProcessedSignal1');
            case 'Res. Frec'
                s=fft(mySignal(:,1));
                hw=pasabajo(length(s),1000,fileReader.SampleRate);
                w=s.*hw';
                y2= real(ifft(w))-imag(ifft(w));
                deviceWriter(app.v*y2);
                scope(app.v*mySignal(:,1),app.v*y2);
            case 'No filter'
                %app.FilterDropDown.Value = 'No filter' ;
                switch app.m 
                    case 'Estereo'
                        deviceWriter(app.v*mySignal);                
                    case 'Mono'
                        deviceWriter(app.v*mySignal(:,1));
                end    
                scope(app.v*mySignal(:,1),app.v*mySignal(:,1));
        end      
    end    
end 

release(scope);
release(fileReader);
release(deviceWriter);