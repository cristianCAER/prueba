file = uigetfile;
fileReader = dsp.AudioFileReader(file);
file1 = uigetfile;
fileReader1 = dsp.AudioFileReader(file1);
deviceWriter = audioDeviceWriter('SampleRate',fileReader1.SampleRate);
scope = dsp.TimeScope(2,...
    'SampleRate',fileReader.SampleRate, ...
    'NumInputPorts',2, ...
    'TimeSpan',0.1, ...
    'BufferLength',1.9e6,...
    'LayoutDimensions',[1,1],...
    'YLimits',[-0.5,0.5]);

app.UIAxes.XLim = [0 fileReader.SampleRate*0.1];
app.UIAxes.YLim = [0 0.2];
app.UIAxes.BackgroundColor = [0.15 0.15 0.15];
app.UIAxes.XColor = [1 1 1]; 

tic;  
app.VolSlider.Value = 0.5;
st = 1;
m1 = 0.1;
m2 = 0.2;
while toc < 30
    A = [str2num(app.a)];
    B = [str2num(app.b)]; 
    stop = app.s
    m1 = app.v
    m2 = -app.v+1
    mySignal = fileReader();
    mySignal1 = fileReader1();
    sum = getsuma(m1*mySignal,m2*mySignal1);
    st = sum(end);
    switch app.g
            case 'No Graphic'
            
            case 'Magnitude'
                [f0,Am,f,m,fase]=getFrec(mySignal,fileReader.SampleRate);
                %[f0,Am]=getfrecuencia(mySignal,fileReader.SampleRate)
                %app.Gauge.Value = app.v*Am;
                if f0 ~= 0              
                    Amplitud = num2str(Am);
                    app.Label_2.Text = Amplitud;
                    Frecuencia = num2str(f0);
                    app.Label.Text = Frecuencia;
                    stem(app.UIAxes,f,m,'.','Color',[0.9 0.132 0.356]);     
                end
            case 'Phase'
        
    end
    switch app.f
            case 'Eq. Dif'
                [myProcessedSignal1,A,B] = IR(sum(:,1),A,B);
                deviceWriter(app.v*myProcessedSignal1');
                scope(m1*sum(:,1),m2*myProcessedSignal1');
            case 'Res. Frec'
                s=fft(sum(:,1));
                size(s)
                hw=pasaalto(length(s),10000,fileReader.SampleRate);
                w=s.*hw';
                y2= real(ifft(w))-imag(ifft(w));
                deviceWriter(app.v*y2);
                scope(m1*sum(:,1),m2*y2);
            case 'No filter'
                %switch  app.m
                 %   case 'Estereo'
                        deviceWriter(app.v*sum);
                 %   case 'Mono'
                        %deviceWriter(app.v*sum(:,1));
               % end                
                scope(m1*sum,m2*sum);
    end
    
end

release(fileReader1);
release(fileReader);
release(deviceWriter);
release(scope);