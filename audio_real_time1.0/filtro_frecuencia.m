clear all
clc
f=49;
fs=1000;
t=0:1/fs:4/f;
y1=sin(2*pi*f*t);
y2=sin(2*pi*11*t);
s=fft(y1);
hw=pasabanda(length(s),10,50,fs);
w=s.*hw;
y2=ifft(w);
plot(t,y2)