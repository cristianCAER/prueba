%% Create input and output objects
deviceReader = audioDeviceReader;
deviceWriter = audioDeviceWriter('SampleRate',deviceReader.SampleRate);
%% Specify an audio processing algorithm
% For simplicity, only add gain.
process = @(x) x.*5;
%% Code for stream processing
disp('Begin Signal Input...')
stop = 0;
scope = dsp.TimeScope(2,...
    'SampleRate',deviceReader.SampleRate,...
    'TimeSpan',0.05,...
    'BufferLength',1.5e6,...
    'YLimits',[-0.3,0.3]);
    %B = [0.2569	-0.7707	0.7707	-0.2569]
    %A = [1 -0.5772 0.4218 -0.0563]
app.UIAxes.XLim = [0 deviceReader.SampleRate*0.1];
app.UIAxes.YLim = [0 1];
app.UIAxes.BackgroundColor = [0.15 0.15 0.15];
app.UIAxes.XColor = [1 1 1];    
while stop ~= 1
    A = [str2num(app.a)];
    B = [str2num(app.b)];   
    stop = app.s
    modo = app.m
    mySignal = deviceReader();
         
        switch app.g
            case 'No Graphic'
                [f0,Am,f,m,fase]=getFrec(mySignal,deviceReader.SampleRate);
                %[f0,Am,f,m,fase]=getfrecuencia(mySignal,deviceReader.SampleRate)
                app.Gauge.Value = app.v*Am;
                if f0 ~= 0              
                    Amplitud = num2str(Am);
                    app.Label_2.Text = Amplitud;
                    Frecuencia = num2str(f0);
                    app.Label.Text = Frecuencia;
                end
            case 'Magnitude'
                [f0,Am,f,m,fase]=getFrec(mySignal,deviceReader.SampleRate);
                %[f0,Am,f,m,fase]=getfrecuencia(mySignal,deviceReader.SampleRate)
                app.Gauge.Value = app.v*Am;
                if f0 ~= 0              
                    Amplitud = num2str(Am);
                    app.Label_2.Text = Amplitud;
                    Frecuencia = num2str(f0);
                    app.Label.Text = Frecuencia;
                    app.UIAxes.YLim = [0 0.07];
                    stem(app.UIAxes,f,m,'.','Color',[0.9 0.132 0.356]);     
                end
            case 'Phase'
                   [f0,Am,f,m,fase]=getFrec(mySignal,deviceReader.SampleRate);
                   app.UIAxes.YLim = [-180 180];
                   size(m)
                  plot(app.UIAxes,f',fase);
                  
        end
    switch app.f
       case 'Eq. Dif'
          %switch app.m
              [myProcessedSignal1,A,B] = IR(mySignal,A,B);
           %   case 'Estereo'               
                deviceWriter(app.v*myProcessedSignal1');          
            %  case 'Mono'            
             %   deviceWriter(app.v*myProcessedSignal1(:,1)');
          %end
          scope(app.v*mySignal(:,1),app.v*myProcessedSignal1');
       case 'Res. Frec'
          s=fft(mySignal(:,1));
          hw=pasaalto(length(s),2000,deviceReader.SampleRate);
          w=s.*hw';
          y2= real(ifft(w))+imag(ifft(w));
          deviceWriter(app.v*y2);
          scope(app.v*mySignal(:,1),app.v*y2);
       case 'No filter'
          switch app.m 
              case 'Estereo'
                deviceWriter(app.v*mySignal);
              case 'Mono'
                deviceWriter(app.v*mySignal(:,1));              
          end
          scope(app.v*mySignal(:,1),app.v*mySignal);            
    end
    %scope(mySignal,myProcessedSignal');
    %plot(myProcessedSignal,'.')
    
end
disp('End Signal Input');

release(deviceReader);
release(deviceWriter);