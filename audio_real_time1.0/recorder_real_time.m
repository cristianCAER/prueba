fs = 200000;
y = audiorecorder(fs,8,1);
b = [0.2569	-0.7707	0.7707	-0.2569];
a = [1 -0.5772 0.4218 -0.0563];
a(2,1:end)=zeros(1,end);%%%%%%%%%%%%%%%%
b(2,1:end)=zeros(1,end);%%%%%%
tic;
cla;
while toc<5
    %a = record()
    recordblocking(y,0.05);
    misdatos = getaudiodata(y,'uint8');
    L = length(misdatos);
    nfft = 2.^nextpow2(L);
    Y = fft(misdatos,nfft)./L;
    f = fs/2*linspace(0,1,nfft/2+1);
    stem(f,2*abs(Y(1:nfft/2+1)),'.','Color',[0.33 0.932 0.256]);
    %hold on
    %plot(misdatos,'Color',[0.53 0.732 0.656]);
    set(gca,'Color','k','GridColor','w','XColor','k','Ycolor','k');
    title('Gr�fica sonido real_time');
    axis([0 fs*0.1/4 0 20])
    ylabel('Amplitude(V)');
    %cla;
    grid on
    pause(0.00000001);
    
end 
