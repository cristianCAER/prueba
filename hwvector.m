function HW=hwvector(x,hw)
nx=length(x);
nw=length(hw);
HW=ones(1,nw);
for i=nx
    HW(i)=x(i)*hw(i);
end