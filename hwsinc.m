function hw=hwsinc(x,fc,fs)
n=length(x);

if n>fs
    x=(0:fs/n:fs);
    x=x(1:end-1);
else
    x=(0:fs/n:fs-1);
end
hw=sinc(x/fc);
hw(n/2+1:end)=0;
hw=hw+flip(hw);
