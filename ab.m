function y=ab(x)
Y=zeros(1,length(x));
for i=length(x)
   if angle(x(i))>=pi/2
     Y(i)=-abs(x(i));
   else
       y(i)=abs(x(i));
   end
end
