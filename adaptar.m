function HW=adaptar(x,hw)
nx=length(x);
nw=length(hw);
if nx~=nw
    if nx>nw
        HW=interp1((1/nw:1/nw:1),hw,(1/nx:1/nx:1),'cubic');
         plot ((1/nw:1/nw:1), hw,'o',(1/nx:1/nx:1), HW)
    else
        while nw<nx
            HW=downsample(hw,2);
            nw=length(hw);
        end
        HW=interp1((1/nw:1/nw:1),hw,(1/nx:1/nx:1),'cubic');
        
    end
else
    HW=hw;
end

    